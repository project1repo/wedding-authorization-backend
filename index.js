import { Datastore } from '@google-cloud/datastore';
import express from 'express';
import cors from 'cors';

const datastore = new Datastore();

const app = express()
app.use(express.json());
app.use(cors())

app.get("/users", async (req,res)=>{
    const query = datastore.createQuery('User');
    const response = await datastore.runQuery(query);
    const newResponse = {"fname":response[0][0].fname, "lname":response[0][0].lname}
    res.send(response);
})

app.patch("/users/login", async (req,res)=>{
    const query = datastore.createQuery('User').filter('email','=',req.body.email).filter('password','=',req.body.password);
    const [data,metaInfo] = await datastore.runQuery(query);
    if(data[0]){
        const returnInfo = {"fname":data[0].fname, "lname":data[0].lname};
        res.status(200);
        res.send(returnInfo);
    }else{
        res.status(404);
        res.send("Username or password does not exist");
    }
    
})

app.get("/users/:email/verify", async(req,res)=>{
    const key = datastore.key(['User', req.params.email]);
    const user = await datastore.get(key);
    
    if(user[0]){
        res.status(200);
        res.send(true);
    }else{
        res.status(404);
        res.send(false);
    }
})


const PORT = process.env.PORT || 3000;

app.listen(PORT, ()=>{console.log("Application Started")});